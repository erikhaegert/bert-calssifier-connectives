import os
import torch
from torch.utils.data import Dataset
from transformers import BertTokenizer
import glob
import pandas as pd 
import pickle as pkl

class ConnDataset(Dataset):
	def __init__(self, path, max_len):
		self.label_dict = {"and":0, "moreover":1, "furthermore":2,"other":3, "no_connective":4 }
		self.data = pkl.load(open("../data/additive_data.p", "rb"))
		#self.data = self._read_in_data(path)
		self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
		self.maxlen = max_len
		self.n_classes = len(self.label_dict)
		
	def __len__(self):
		return len(self.data)

	def __getitem__(self, index):
		#get data
		#print(index)
		first = self.data.loc[index, "first"]
		second = self.data.loc[index, "second"]
		label = self.label_dict[self.data.loc[index, "label"]]
		onehot = torch.zeros(self.n_classes)
		#print(onehot)
		onehot[label]=1
		#tokenize sentence pair
		first_tokens = self.tokenizer.tokenize(first)
		second_tokens = self.tokenizer.tokenize(second)
		#Add special tokens
		tokens = ["[CLS]"] + first_tokens + ["[SEP]"] + second_tokens + ["[SEP]"]
		#padd sequences
		if len(tokens) < self.maxlen:
			tokens = tokens + ['[PAD]' for _ in range(self.maxlen-len(tokens))]
		#or prune it
		else:
			tokens = tokens[:self.maxlen-1]+["[SEP]"]
		#get indices of tokens in Bert Vocabulary
		token_ids =  self.tokenizer.convert_tokens_to_ids(tokens)
		#convert list to torch tensor
		token_ids_tensor = torch.tensor(token_ids) 
		#get attention mask
		attention_mask = (token_ids_tensor !=0).long()
		#get segment_ids 
		segment_ids = [0 if i < len(first_tokens)+ 2 else 1 for i in range(self.maxlen)]
		segment_ids = torch.tensor(segment_ids)
		return token_ids_tensor, segment_ids, attention_mask, onehot

	def _read_in_data(self,path):
		all_files = glob.glob(os.path.join(path, "*.tsv"))     # advisable to use os.path.join as this makes concatenation OS independent
		dfs = (pd.read_csv(f, names = ["first", "label", "second","source"], header = None, delimiter = "\t") for f in all_files)
		out_df   = pd.concat(dfs, ignore_index=True)
		#out_df = out_df.drop(out_df["label"] not in self.label_dict.keys())
		#print(out_df)
		#spkl.dump(out_df, open("../data/additive_data.p", "wb"))
		pkl.dump(out_df, open("../data/toy_data.p", "wb"))

		return out_df
