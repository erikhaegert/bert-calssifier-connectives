import sys
import torch
import numpy as np
from datetime import datetime
from torch.utils.data import DataLoader, SubsetRandomSampler
from data_set import ConnDataset
import torch.nn as nn
import torch.optim as optim
from sklearn.metrics import classification_report, accuracy_score
from bert_model import ConnInserter

#generates and splits data into train and dev sets of padded batches
def split_data(data, val_split =0.1, random_seed = 42, batch_size =64, shuffle_dataset = True):
	dataset_size = len(data)
	indices = list(range(dataset_size))
	split = int(np.floor(val_split * dataset_size))
	if shuffle_dataset:
		np.random.seed(random_seed)
		np.random.shuffle(indices)
	train_indices, val_indices = indices[split:], indices[:split]
	train_sampler = SubsetRandomSampler(train_indices)
	valid_sampler = SubsetRandomSampler(val_indices)
	train_loader = DataLoader(data,
								batch_size=batch_size, 
								sampler=train_sampler 
								)
	validation_loader = DataLoader(data,
									batch_size=batch_size,
									sampler=valid_sampler 
									)
	return train_loader, validation_loader

def evaluate(val_loader, model, criterion, all = False, its=4):
	preds =[]
	truth = []
	dev_loss = []
	with torch.no_grad():
		model.eval()
		for batch_id, (token_ids_tensor, segment_ids, attention_mask, labels) in enumerate(val_loader):
			logits = model(token_ids_tensor, segment_ids, attention_mask)
			loss = criterion(logits.squeeze(-1), labels.float())
			dev_loss.append(loss.item())
			preds += torch.argmax(logits, axis=1).tolist()
			truth += torch.argmax(labels, axis=1).tolist()
			if all==False and batch_id == its:			
				print(classification_report(truth,preds))
				print("DEV ACC: ", accuracy_score(truth,preds))
				return
		print(classification_report(truth,preds))
		print("DEV ACC: ", accuracy_score(truth,preds))
		return  


def train(data_path="../data/additive/", num_epochs = 3, maxlen=40):
	data_set = ConnDataset(path=data_path, max_len= maxlen)
	#val_set = ConnDataset(path=data_path, max_len = maxlen)
	print("created_sets... ")
	train_loader, val_loader = split_data(data_set)
	print("loaded_sets...")
	net = ConnInserter(freeze_bert=True)
	print("loaded model... ")
	criterion = nn.BCEWithLogitsLoss()
	opti = optim.Adam(net.parameters(), lr = 2e-5)
	print("starting training... ")
	for epoch in range(num_epochs):
		print("epoch {}".format(epoch))
		for i, (token_ids_tensor, segment_ids, attention_mask, labels) in enumerate(train_loader):
			opti.zero_grad()
			#mit cuda:
			# seq, seg_ids, attn_masks, labels = tokens_ids_tensor.cuda(args.gpu), segment_ids.cuda(args.gpu), attention_masks.cuda(args.gpu), label.cuda(args.gpu)
			logits = net(token_ids_tensor, segment_ids, attention_mask)
			loss = criterion(logits.squeeze(-1), labels.float())
			loss.backward()
			opti.step()
			if (i + 1) % 10 == 0:
				#print(logits.shape)
				preds = torch.argmax(logits, axis=1).tolist()
				truth = torch.argmax(labels, axis=1).tolist()
				#print(preds)
				#print(truth)
				print(classification_report(truth,preds))
				print("Iteration {} of epoch {} complete. Loss : {} Accuracy : {}".format(i+1, epoch+1, loss.item(), accuracy_score(truth,preds)))	
			if (i + 1) % 100 == 0:
				evaluate(val_loader, net, criterion)
				
		evaluate(val_loader, net, criterion, all = True)
	evaluate(val_loader, net, criterion, all = True)
	#save
	tim = datetime.now()
	date = "".join(str(tim).split())
	date = date.replace(":", "-")
	with open("../chkpt/bert_model-" + date + ".p", "wb") as f:
		torch.save(net, f)




if __name__ == "__main__":
	train(data_path="../data/additive_data.p", num_epochs=3)
