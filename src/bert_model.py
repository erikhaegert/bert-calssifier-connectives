import torch
import torch.nn as nn 
from transformers import BertModel




class ConnInserter(nn.Module):
	def __init__(self, freeze_bert = True):
		super(ConnInserter, self).__init__()
		self.bert_layer = BertModel.from_pretrained('bert-base-uncased')


		if freeze_bert:
			for p in self.bert_layer.parameters():
				p.requires_grad = False

		
		self.cls_layer = nn.Linear(768, 5)
		#self.predictions = []

	#def init_predictions(self):
	#	self.predictions = []

	def forward(self, token_ids, segment_ids, attention_mask):
		#self.predictions = []
		#get contextualized representations
		cont_reps, _ = self.bert_layer(token_ids, token_type_ids=segment_ids, attention_mask =attention_mask)
		#get representation of CLS-head
		cls_rep = cont_reps[:,0]
		#feed representation of cls-heat to classifier layer
		logits = self.cls_layer(cls_rep)
		#collect predictions
		#self.predictions.append(torch.argmax(logits))
		return logits
